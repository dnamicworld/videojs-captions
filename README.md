# videojs-closed-captions

Add, remove and update captions

## Table of Contents

<!-- START doctoc -->
<!-- END doctoc -->
## Installation

```sh
npm install --save videojs-closed-captions
```

## Usage

To include videojs-closed-captions on your website or web application, use any of the following methods.

### `<script>` Tag

This is the simplest case. Get the script in whatever way you prefer and include the plugin _after_ you include [video.js][videojs], so that the `videojs` global is available.

```html
<script src="//path/to/video.min.js"></script>
<script src="//path/to/videojs-closed-captions.min.js"></script>
<script>
  var player = videojs('my-video');

  player.closedCaptions();
</script>
```

### Browserify

When using with Browserify, install videojs-closed-captions via npm and `require` the plugin as you would any other module.

```js
var videojs = require('video.js');

// The actual plugin function is exported by this module, but it is also
// attached to the `Player.prototype`; so, there is no need to assign it
// to a variable.
require('videojs-closed-captions');

var player = videojs('my-video');

player.closedCaptions();
```

### RequireJS/AMD

When using with RequireJS (or another AMD library), get the script in whatever way you prefer and `require` the plugin as you normally would:

```js
require(['video.js', 'videojs-closed-captions'], function(videojs) {
  var player = videojs('my-video');

  player.closedCaptions();
});
```

## License

MIT. Copyright (c) Jairo Campos


[videojs]: http://videojs.com/
