class Captions {
  constructor(player, options) {
    this.captions = options.captions;
    this.player = player;
    this.currentTime = 0;
    this.currentLang = 'en';
    this.languages = [];
    this.customButton = false;
    this.active = true;
    this.setLanguages();
    this.buildContainer();
    this.events();
    this.addCaptionButton();
  }
  setLanguages () {
    this.languages = [...new Set(this.captions.map(item => item.language))];
  }
  updateLanguages(languages) { 
    this.languages = languages;
  }
  updatesCaptions(captions) {
      console.log('called');
      this.captions = captions;
      const caption = this.activeCaption();

      this.showCaption(caption);
  }
  selectLang(lang) {
    this.currentLang = lang;
  }
  hasCaptions () {
    return (this.captions.length > 0 ) ? true : false;
  }
  buildContainer() {
    this.captionContainer = document.createElement('div');
    this.captionContainer.className = 'vjs-caption-container';
    this.player.el().appendChild(this.captionContainer);
  }
  showCaption(caption) {
    this.captionContainer.innerHTML = (caption.length > 0) ? caption[0].content : '';
  }
  activeCaption() {  
    return this.captions.filter(caption =>
    caption.start_time <= this.currentTime && caption.end_time >= this.currentTime && this.currentLang === caption.language.code);
  }
  disable() {
    this.active = false;
    this.captionContainer.innerHTML = '';
  }
  enable() {
    this.active = true;
  }
  events() {
    this.player.on('timeupdate', () => {
      if (this.active) {
        this.currentTime = this.player.currentTime();
        const currentCaption = this.activeCaption();

        this.showCaption(currentCaption);
      }

      if (this.hasCaptions()) {
        this.addActiveItem ();  
        this.addLangItems(); 
      }
    });
  }
  addCaptionButton() {
    if (!this.customButton) {
        const captionButton = this.player.controlBar.addChild('button',
        {
            'el' : videojs.createEl('button', { className : 'vjs-control  vjs-icon-captions vjs-icon-captions-extends'}),
        });
        this.customButton = true;
        this.addCaptionMenu();
    }
  }
  addLangItems() {
    const that = this; 
    for (let i = 0; i < this.languages.length; i++ ) { 
      if (this.player.el().getElementsByClassName(`vjs-item-${this.languages[i].code}`).length === 0) {  
        const langItem = document.createElement('li');

        langItem.className = `vjs-item-lang vjs-item-${this.languages[i].code}`;
        langItem.setAttribute('data-code', this.languages[i].code);
        langItem.innerHTML = this.languages[i].name;
        this.menu.appendChild(langItem);

        langItem.addEventListener("click",function(e){
            that.currentLang = this.getAttribute('data-code');
            const caption = that.activeCaption();

            that.showCaption(caption);
        });
      }
    }
  }
  addActiveItem () {
    if (this.player.el().getElementsByClassName('vjs-item-active').length === 0) {
      const activeItem = document.createElement('li');

      activeItem.className = 'vjs-item-active';
      activeItem.innerHTML = 'Active caption';
      this.menu.appendChild(activeItem);
    }
  }
  addCaptionMenu() {
      const button = this.player.el().getElementsByClassName('vjs-icon-captions-extends')[0];
      this.menu = document.createElement('ul');
      const editOption = document.createElement('li');

      this.menu.className = 'vjs-caption-menu';
      editOption.innerHTML = 'Caption edit';
      editOption.className = 'vjs-caption-menu-item vjs-edit-option';
      this.menu.appendChild(editOption);
      button.appendChild(this.menu);
  }
}
export default Captions;
